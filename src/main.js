// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import App from './App';
// import Store from './components/Store';
import Start from './components/Start';
import Game from './components/Game';
import Message from './components/Message';
import End from './components/End';
import Share from './components/Share';
import Goal from './components/Goal';
import Vuex from 'vuex';
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.http.headers.common.Authorization = 'Basic Y21wYzo0Y2Q5YjlkNDZhZDk0MTAxMjAzZjRkYWU4OGQ1MDBkYjc3NjNkMGEx';

const routes = [
  { path: '/', component: Start, name: 'start' },
  { path: '/game', component: Game, name: 'game' },
  { path: '/message/:id', component: Message, name: 'message' },
  { path: '/end', component: End, name: 'end' },
  { path: '/share', component: Share, name: 'share' },
  { path: '/goal', component: Goal, name: 'goal' },
];

const router = new VueRouter({
  routes,
});

/* eslint-disable no-param-reassign */

const store = new Vuex.Store({
  state: {
    count: 0,
    carousel: 0,
    timer: 18,
    lives: 6,
    computo: 0,
    franstatus: 1,
    words: [
      'LIMPIA',
      'ABSORBE',
      'TRAPEA',
      'HIGIENIZA',
      'ABRILLANTA',
      'ARREGLA',
      'ENFRIA',
      'SECA',
      'ACOMPAÑA',
      'APOYA',
      'PRACTICA',
      'DESECHABLE',
      'LUSTRA',
    ],
  },
  mutations: {
    increment(state) {
      state.count++;
    },
    carousel(state, n) {
      state.carousel = n;
    },
    decreaseTimer(state) {
      state.timer--;
    },
    decreaseLives(state) {
      state.lives--;
    },
    resetTimer(state) {
      state.timer = 18;
    },
    franidle(state) {
      console.log(state.franstatus);
      state.franstatus = (state.franstatus === 1) ? 0 : 1;
    },
    franset(state, r) {
      state.franstatus = r;
    },
    computoSet(state, c) {
      state.computo = c >= 10000 ? 10000 : c;
    },
  },
});

/* eslint-disable no-new */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable one-var */
/* globals FB */

const fbapi = function fbapi() {
  window.fbAsyncInit = function fbAsyncInit() {
    FB.init({
      appId: '1855815304641619',
      xfbml: true,
      cookie: true,
      version: 'v2.8',
    });
  };
  (function b(d, s, id) {
    let js = null;
    const fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/es_LA/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
};

console.log('%c ███╗   ███╗ █████╗ ██████╗\r\n ████╗ ████║██╔══██╗██╔══██╗\r\n ██╔████╔██║███████║██║  ██║\r\n ██║╚██╔╝██║██╔══██║██║  ██║\r\n ██║ ╚═╝ ██║██║  ██║██████╔╝\r\n ╚═╝     ╚═╝╚═╝  ╚═╝╚═════╝\r\n', 'background: #000000; color: #ff9529');
console.log('%c ¿Buscas pega? aacuna [at] mad.cl', 'background: #000000; color: #ff9529');

new Vue({
  router,
  store,
  created: fbapi,
  el: '#app',
  render: h => h(App),
});
